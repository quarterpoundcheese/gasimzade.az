import ResponsiveBox from "./ResponsiveBox";
import {Box, Flex, Grid, Heading, Stack, Text} from "@chakra-ui/react";
import EduExpData, {EduExpColumn, EduExp} from "../data/eduExp";
import StackData from "../data/stack";

interface EduExpItemProps {
    item: EduExp,
}


interface EduExpColumnProps {
    column: EduExpColumn,
}


function EduExpItem(props: EduExpItemProps) {
    return (
        <Box>
            <Heading as={'h2'} fontFamily={"PT Serif"} fontSize={18}>{props.item.title}</Heading>
            <Text mt={1} fontFamily={"Poppins"} color={'gray.400'} fontSize={12}>{props.item.location}</Text>
        </Box>
    )
}

function EduExpColumn(props: EduExpColumnProps) {
    return (
        <Box>
            <Text fontFamily={"Poppins"} color={'gray.400'}>{props.column.title}</Text>
            <Stack mt={8} spacing={8}>
                {
                    props.column.items.map((item, key) => (
                        <EduExpItem item={item} key={key} />
                    ))
                }
            </Stack>
        </Box>
    )
}

export default function EduExpStack() {
    return (
        <ResponsiveBox id={"experience"} mx={'auto'} py={12} mt={3} borderTop={"1px solid #ccc"}>
            <Heading as={'h1'} textAlign={'center'} fontFamily={"PT Serif"} fontWeight={300}>Education and Experience</Heading>
            <Box mt={10}>
                <Flex w={"100%"} justifyContent={'space-between'} flexDirection={['column', 'column', 'row']}>
                    {EduExpData.map((item, key) => (
                        <Box p={2} py={4} key={key}>
                            <EduExpColumn column={item} />
                        </Box>
                    ))}
                </Flex>
            </Box>
        </ResponsiveBox>
    )
}