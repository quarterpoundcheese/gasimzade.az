import {Grid, Text, HStack, Link} from "@chakra-ui/react";
import ResponsiveBox from "./ResponsiveBox";
import SocialLinks from "./SocialLink";

export default function Footer() {
    return (
        <ResponsiveBox mx={'auto'} py={12} mt={3} borderTop={"1px solid #ccc"}>
            <Grid gridTemplateColumns={["1fr", "1fr", "1fr 1fr 1fr"]} fontSize={14} color={"gray.400"}>
                <Text p={2} textAlign={['center', 'center', 'left']} fontFamily={"Poppins"}>
                    © {new Date().getFullYear()} Ali Gasimzade, All Rights Reserved
                </Text>
                <Link p={2} textAlign={'center'} _hover={{color: "black"}} transition={'200ms'} href={"mailto:ali@gasimzade.az"}>
                    ali@gasimzade.az
                </Link>
                <SocialLinks ml={['none', 'none', 'auto']} mx={['auto', 'none', 'none']}/>
            </Grid>
        </ResponsiveBox>
    )
}