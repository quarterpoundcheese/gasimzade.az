import React from 'react';
import Head from 'next/head';

export default function HeadComponent() {

    return (
        <Head>
            <title>{`Full-stack Developer | Ali Gasimzade`}</title>
            <meta name="title" content={`Full-stack Developer | Ali Gasimzade`}/>
            <meta name="description"
                  content="Ali Gasimzade - a Full-Stack Engineer Based in Azerbaijan"/>
            <meta name="keywords" content={"web design, node js, react, next, front-end"}/>

            <meta property="og:type" content="website"/>
            <meta property="og:url" content="https://gasimzade.az"/>
            <meta property="og:title" content={`Full-stack Developer | Ali Gasimzade`}/>
            <meta property="og:description"
                  content="Ali Gasimzade - a Full-Stack Engineer Based in Azerbaijan"/>
            <meta property="og:image"
                  content="https://gasimzade.az/display.png"/>

            <meta property="twitter:card" content="summary_large_image"/>
            <meta property="twitter:url" content="https://gasimzade.az"/>
            <meta property="twitter:title"
                  content={`Full-stack Developer | Ali Gasimzade`}/>
            <meta property="twitter:description"
                  content="Ali Gasimzade - a Full-Stack Engineer Based in Azerbaijan"/>
            <meta property="twitter:image"
                  content="https://gasimzade.az/display.png"/>
        </Head>
    )
}