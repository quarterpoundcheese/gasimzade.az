import {Box, Button, Heading, Link, Stack} from "@chakra-ui/react";
import ResponsiveBox from "./ResponsiveBox";
import HeroStats from "./HeroStats";

export default function Hero() {
    return (
        <Box mt={12}>
            <ResponsiveBox mx={'auto'} textAlign={'center'}>
                <Box>
                    <Stack spacing={1}>
                        <Heading as={'h1'} fontWeight={300} fontSize={[26, 30, 38]} fontFamily={"PT Serif"}>
                            Ali Gasimzade
                        </Heading>
                        <Heading as={'h1'} fontWeight={300} fontSize={[26, 30, 38]} fontFamily={"PT Serif"}>
                            Full-Stack Engineer
                        </Heading>
                        <Heading as={'h1'} fontWeight={300} fontSize={[26, 30, 38]} fontFamily={"PT Serif"}>
                            Based in Azerbaijan
                        </Heading>
                    </Stack>
                    <Box my={8}>
                        <Link href={"/Ali Gasimzade.pdf"} download border={"1px solid #ccc"} fontWeight={'bold'} fontSize={12} textTransform={"uppercase"} fontFamily={"Poppins"} padding={2} borderRadius={12}>
                            Download Resume
                        </Link>
                    </Box>
                </Box>
                <Box mt={12} mb={16}>
                    <HeroStats/>
                </Box>
            </ResponsiveBox>
        </Box>
    )
}