import {Box, Grid, Heading, Img, Stack, Text} from "@chakra-ui/react";
import dayjs from "dayjs";
import HeadComponent from "./HeadComponent";
import StatData, {StatProps} from '../data/heroStats';

function Stat(props: StatProps) {
    return (
        <Box>
            <Heading textAlign={props.right ? 'right' : 'left'} fontSize={12} color={'gray'} textTransform={'uppercase'}
                     as={'h3'}>{props.title}</Heading>
            <Box mt={6} ml={props.right ? 'auto' : 0}>
                {props.content.split("\n").map((text, key) => (
                    <Text textAlign={props.right ? 'right' : 'left'} fontSize={props.fontSize ?? 16}
                          key={key}>{text}</Text>
                ))}
            </Box>
        </Box>
    )
}

export default function HeroStats() {
    return (
        <Box mx={'auto'}>
            <HeadComponent />
            <Box display={['none', 'none', 'block']}>
                <Grid width={"100%"} gridGap={8} justifyContent={"center"} gridTemplateColumns={"1fr 2fr 1fr"}>
                    <Stack mr={'auto'} spacing={12}>
                        {
                            StatData.filter((f) => !f.right).map((t, k) => <Stat title={t.title} key={k} content={t.content} fontSize={t.fontSize} />)
                        }
                    </Stack>
                    <Box>
                        <Box bgSize={"cover"} bgPosition={"center"} height={"100%"} borderRadius={120} bgImage={"/ali.webp"} w={""} />
                    </Box>
                    <Stack ml={'auto'} spacing={12}>
                        {
                            StatData.filter((f) => f.right).map((t, k) => <Stat title={t.title} key={k} content={t.content} fontSize={t.fontSize} right />)
                        }
                    </Stack>
                </Grid>
            </Box>
            <Box display={['block', 'block', 'none']}>
                <Stack spacing={8}>
                    {
                        StatData.map((t, k) => <Stat title={t.title} key={k} content={t.content} fontSize={t.fontSize} />)
                    }
                </Stack>
            </Box>
        </Box>
    )
}
