import {Box, Grid, Heading, HStack, Img, Link, Square, Text} from "@chakra-ui/react";
import ResponsiveBox from "./ResponsiveBox";
import Projects, {Project} from '../data/projects';

interface SingleWorkProps {
    item: Project
}

function SingleWork({item}: SingleWorkProps) {
    return (
        <Link href={item.link} target={"_blank"}>
            <Box>
                <Box boxShadow={"sm"} borderRadius={8} height={"200px"} alt={item.title} bgSize={"cover"} bgPosition={"center"} bgImage={item.image} />
                <Text mt={1} fontSize={12} fontFamily={"Poppins"}>
                    {
                        item.keywords.join(", ")
                    }
                </Text>
                <Text mt={1}>{item.title}</Text>
            </Box>
        </Link>
    )
}

export default function LatestWork() {
    return (
        <ResponsiveBox id={"works"} mx={'auto'} py={12} mt={3} borderTop={"1px solid #ccc"}>
            <Heading as={'h1'} textAlign={'center'} fontFamily={"PT Serif"} fontWeight={300}>Latest Work</Heading>
            <Box mt={10}>
                <Grid w={"100%"} gridGap={8} gridTemplateColumns={["1fr", "1fr 1fr", "1fr 1fr 1fr"]}>
                    {
                        Projects.map((project, key) => (
                            <SingleWork item={project} key={key} />
                        ))
                    }
                </Grid>
            </Box>
        </ResponsiveBox>
    )
}