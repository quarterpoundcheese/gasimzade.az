import {Box, Flex, Grid, Heading, HStack, Img, Link, Spacer} from "@chakra-ui/react";
import {useCallback, useState} from "react";
import { FaBars } from "react-icons/fa";
import ResponsiveBox from "./ResponsiveBox";
import SocialLinks from "./SocialLink";
import {AnimatePresence, motion} from 'framer-motion';

const navigationItems: {title: string, elementId: string}[] = [
    {
        title: "Skills",
        elementId: "skills",
    },
    {
        title: "Experience",
        elementId: "experience",
    },
    {
        title: "Works",
        elementId: "works",
    }
]

export default function Navbar() {

    const [isOpen, setIsOpen] = useState(false);

    const navigateTo = useCallback((elementId: string) => {
        document.getElementById(elementId)?.scrollIntoView({behavior: "smooth"});
        setTimeout(() => {
            location.replace("#"+elementId)
        }, 500)
    }, [])


    return (
        <ResponsiveBox py={8} mx={'auto'} borderBottom={"1px solid #ccc"}>
            <Box display={['none', 'none', 'block']}>
                <Grid gridTemplateColumns={["1fr 1fr 1fr"]} alignItems={'center'}>
                    <HStack mr={'auto'} textTransform={"uppercase"} fontWeight={'bold'} spacing={10}>
                        {
                            navigationItems.map((f, k) => (
                                <Link onClick={() => navigateTo(f.elementId)} key={k}>
                                    <Heading as={'h1'} fontSize={13}>{f.title}</Heading>
                                </Link>
                            ))
                        }
                    </HStack>
                    <Img src={"/alogo.png"} alt={'ali gasimzade logo'} w={"60px"} mx={'auto'} />
                    <SocialLinks ml={'auto'} />
                </Grid>
            </Box>
            <Box display={['block', 'block', 'none']}>
                <Flex alignItems={'center'}>
                    <Img src={"/alogo.png"} alt={'ali gasimzade logo'} w={"40px"} mx={'auto'} />
                    <Spacer />
                    <Box pb={1} pl={1} pt={1} onClick={() => setIsOpen(!isOpen)}>
                        <motion.div animate={{rotate: isOpen ? 90 : 0}}>
                            <FaBars size={28} />
                        </motion.div>
                    </Box>
                </Flex>
                <AnimatePresence >
                    {
                        isOpen && (
                            <motion.div initial={{maxHeight: 0, opacity: 0}} animate={{maxHeight: 300, opacity: 1}} exit={{maxHeight: 0, opacity: 0}}>
                                {
                                    navigationItems.map((f, k) => (
                                        <Link p={4} onClick={() => navigateTo(f.elementId)} key={k}>
                                            <Heading textAlign={"center"} as={'h1'} fontSize={13}>{f.title}</Heading>
                                        </Link>
                                    ))
                                }
                                <Box mx={'auto'} w={'fit-content'}>
                                    <SocialLinks />
                                </Box>
                            </motion.div>
                        )
                    }
                </AnimatePresence>
            </Box>
        </ResponsiveBox>
    )
}