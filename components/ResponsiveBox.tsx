import {Box, ChakraProps} from "@chakra-ui/react";


export default function ResponsiveBox(props: any) {
    return (
        <Box {...props} w={['90%', '80%', "75%"]} maxWidth={"1920px"}>
            {props.children}
        </Box>
    )
}