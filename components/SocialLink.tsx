import {ChakraProps, Box, HStack, Link} from "@chakra-ui/react";
import {FaFacebookF, FaGithub, FaLinkedinIn, FaGitlab, FaGit} from "react-icons/fa";

export default function SocialLinks(props: ChakraProps) {
    return (
        <Box {...props}>
            <HStack textTransform={"uppercase"} fontWeight={'bold'} spacing={5}>
                <Link _hover={{color: "black"}} p={2} transition={"200ms"}
                      href={"https://www.facebook.com/ali.qasimzade.3"} target={"_blank"}>
                    <FaFacebookF/>
                </Link>
                <Link _hover={{color: "black"}} p={2} transition={"200ms"}
                      href={"https://www.linkedin.com/in/ali-gasimzade-9815a117a"} target={"_blank"}>
                    <FaLinkedinIn/>
                </Link>
                <Link _hover={{color: "black"}} p={2} transition={"200ms"} href={"https://github.com/quarterpound"}
                      target={"_blank"}>
                    <FaGithub/>
                </Link>
                <Link _hover={{color: "black"}} p={2} transition={"200ms"} href={"https://gitlab.com/quarterpoundcheese"}
                      target={"_blank"}>
                    <FaGitlab/>
                </Link>
            </HStack>
        </Box>

    )
}