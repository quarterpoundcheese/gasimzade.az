import {Box, Tooltip, Text, Heading, Img, Center, Grid} from "@chakra-ui/react";
import ResponsiveBox from "./ResponsiveBox";
import StackData, {Stack} from '../data/stack';

interface SingleStackProps {
    item: Stack,
}

function SingleStack(props: SingleStackProps) {
    return (
        <Box>
            <Box bgColor={"gray.100"} px={8} py={8} borderRadius={30}>
                <Box height={"100px"}>
                    <Center>
                        <Tooltip label={props.item.name}>
                            <Img src={props.item.image} width={"100px"} height={"100px"}/>
                        </Tooltip>
                    </Center>
                </Box>
                <Box mt={3}>
                    <Text fontSize={24} textAlign={'center'}>{props.item.percent}%</Text>
                </Box>
            </Box>
            <Box mt={6}>
                <Text textAlign={'center'}>{props.item.name}</Text>
            </Box>
        </Box>
    )
}

export default function TechStack() {
    return (
        <ResponsiveBox id={"skills"} mx={'auto'} py={12} mt={3} borderTop={"1px solid #ccc"}>
            <Heading as={'h1'} textAlign={'center'} fontFamily={"PT Serif"} fontWeight={300}>Tech Stack</Heading>
            <Box mt={10}>
                <Grid w={"100%"} gridGap={4} gridTemplateColumns={["1fr", "1fr 1fr 1fr", "1fr 1fr 1fr 1fr", StackData.map(() => "1fr ").join("")]}>
                    {StackData.map((item, key) => (
                        <SingleStack item={item} key={key}/>
                    ))}
                </Grid>
            </Box>
        </ResponsiveBox>
    )
}