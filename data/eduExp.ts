export interface EduExp {
    title: string,
    location: string,
}

export interface EduExpColumn {
    title: string,
    items: EduExp[]
}

const data: EduExpColumn[] = [
    {
        title: "2017 - current",
        items: [
            {
                title: "Bachelors of Science in Computer Science",
                location: "ADA University, Baku Azerbaijan"
            },
            {
                title: "Bachelors of Science in Computer Science (Exchange Semester)",
                location: "University for Information Science and Technology, Ohrid Macedonia"
            },
            {
                title: "Bachelors of Science in Applied Mathematics (Second Degree)",
                location: "Baku State University, Baku Azerbaijan"
            }
        ]
    },
    {
        title: "2017 - 2021",
        items: [
            {
                title: "Webmaster / Front-end Developer",
                location: "ESN Azerbaijan"
            },
            {
                title: "Full-stack Developer",
                location: "Azmo Solutions LLC"
            },
            {
                title: "Front-end Developer",
                location: "Optima Business Solutions LLC"
            }
        ]
    },
    {
        title: "2022 - current",
        items: [
            {
                title: "Front-end Developer",
                location: "Teqqed"
            },
        ]
    }
]
export default data;