import dayjs from "dayjs";

export interface StatProps {
    title: string,
    content: string,
    right?: boolean,
    fontSize?: number,
}

const data:StatProps[] = [
    {
        title: "biography",
        content: "Work for money and design for love!\nI'm Ali, a full-stack engineer based\n in Azerbaijan",
    },
    {
        title: "contact",
        content: 'Baku, Azerbaijan\ngasimzadeali@gmail.com\n+994 55 699 60 30',
    },
    {
        title: "services",
        content: "Back-end development\nWeb & Mobile Development\nApplication Development\n",
    },
    {
        title: "years of experience",
        content: (dayjs().year() - 2017 - 1).toString(),
        fontSize: 33,
        right: true,
    },
    {
        title: "projects completed",
        content: "15+",
        fontSize: 33,
        right: true,
    }
] 

export default data;