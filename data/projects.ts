export interface Project {
    title: string,
    keywords: string[],
    image: string,
    link?: string
}

const data: Project[] = [
    {
        title: "Smeta Normativ Qiymət Qoyma İnformasiya Sistemi",
        keywords: ["PHP", "React", "Nginx"],
        image: "/projects/snq.webp",
        link: "https://snqis.gov.az"
    },
    {
        title: "Unicons",
        keywords: ["Next JS", "Strapi", "Nginx"],
        image: "/projects/unicons.webp",
        link: "https://unicons.az"
    },
    {
        title: "Profsolutions",
        keywords: ["Next JS", "Strapi", "Nginx"],
        image: "/projects/profsolutions.webp",
        link: "https://profsolutions.az"
    },
    {
        title: "Ordubad Jams",
        keywords: ["Next JS", "Strapi", "E-Commerce", "Kapital Bank"],
        image: "/projects/ordubad.webp",
        link: "https://ordubadjams.com"
    },
    {
        title: "Asso Project",
        keywords: ["Next JS", "Strapi"],
        image: "/projects/asso.webp",
        link: "https://assoproject.az"
    },
    {
        title: "Azmo Solutions",
        keywords: ["Next JS", "Node JS", "React"],
        image: "/projects/azmo.webp",
        link: "https://azmo.az"
    },
    {
        title: "Solong Live",
        keywords: ["Next JS", "Strapi"],
        image: "/projects/solong.webp",
        link: "https://solong.live"
    },
    {
        title: "AES",
        keywords: ["Next JS", "Strapi", "Nginx"],
        image: "/projects/aes.webp",
        link: "https://aes.az"
    },
]

export default data;
