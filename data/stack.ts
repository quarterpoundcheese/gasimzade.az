export interface Stack {
    image: string,
    percent: number,
    name: string
}

const data: Stack[] = [
    {
        name: "Nest",
        percent: 90,
        image: "/stack/nest.png",
    },
    {
        name: "Javascript",
        percent: 100,
        image: "/stack/js.png",
    },
    {
        name: "React",
        percent: 100,
        image: "/stack/atom.png",
    },
    {
        name: "Go",
        percent: 70,
        image: "/stack/go.png",
    },
    {
        name: "Git",
        percent: 100,
        image: "/stack/git.png",
    },
    {
        name: "Typescript",
        percent: 100,
        image: "/stack/ts.png",
    },
]

export default data;