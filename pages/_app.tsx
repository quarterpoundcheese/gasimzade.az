import type {AppProps} from 'next/app'
import {ChakraProvider, extendTheme} from "@chakra-ui/react";
import "@fontsource/pt-serif"
import "@fontsource/poppins"

const theme = extendTheme({
    fonts: {
        body: "PT Serif",
        heading: "Poppins"
    },
    styles: {
        global: {
            body: {
                bg: "#f6f6f6"
            }
        }
    }
})

function MyApp({Component, pageProps}: AppProps) {
    return (
        <ChakraProvider theme={theme}>
            <Component {...pageProps} />
        </ChakraProvider>
    )
}

export default MyApp
