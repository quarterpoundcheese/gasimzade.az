import type {NextPage} from 'next'
import {Box} from "@chakra-ui/react";
import Navbar from "../components/Navbar";
import Hero from "../components/Hero";
import TechStack from "../components/TechStack";
import EduExpStack from "../components/EduExpStack";
import LatestWork from "../components/LatestWork";
import Footer from "../components/Footer";

const Home: NextPage = () => {
    return (
        <Box>
            <Navbar />
            <Hero />
            <TechStack />
            <EduExpStack />
            <LatestWork />
            <Footer />
        </Box>
    )
}

export default Home
